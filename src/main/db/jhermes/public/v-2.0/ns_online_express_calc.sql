--liquibase formatted sql
--changeset bogomolov:ns_online_express_calc splitStatements:false failOnError:true
 insert into actions( id, actiontype_id, descr, dependant, flatten_params )
values ( 'products/ns/expressCalc', 'call', 'Расчет экспресс-котировки НС-Онлайн', 'USER_NSOnlineExpressCalc', false );

 insert into tparams( id, kias_name, descr, type_id, public_name, maxlength, mask, parent_id ) values
( 'term-begin', 'term-begin', 'Первый день действия договора', 'date', 'term-begin', null, null, null ),
( 'term-end', 'term-end', 'Последний день действия договора', 'date', 'term-end', null, null, null ),
( 'coverage-risks-risk-id', 'coverage-risks-risk-id', 'Идентификатор покрытия риска', 'string', 'coverage-risks-risk-id', null, null, null ),
( 'coverage-risks-risk-value', 'coverage-risks-risk-value', 'Значение покрытия риска', 'string', 'coverage-risks-risk-value', null, null, null ),
( 'attributes-attribute-id', 'attributes-attribute-id', 'Идентификатор атрибута', 'string', 'attributes-attribute-id', null, null, null ),
( 'attributes-attribute-value', 'attributes-attribute-value', 'Значение атрибута', 'string', 'attributes-attribute-value', null, null, null ),
( 'insurants-insurant', 'insurants-insurant', null, null, 'insurants-insurant', null, null, 'insured_age' );

 insert into action_params( action_id, param_id, is_required ) values
 ( 'products/ns/expressCalc', 'term-begin', true ),
 ( 'products/ns/expressCalc', 'term-end', true ),
 ( 'products/ns/expressCalc', 'coverage-risks-risk-id', true ),
 ( 'products/ns/expressCalc', 'coverage-risks-risk-value', true ),
 ( 'products/ns/expressCalc', 'attributes-attribute-id', true ),
 ( 'products/ns/expressCalc', 'attributes-attribute-value', true ),
 ( 'products/ns/expressCalc', 'insurants-insurant', true );

 insert into roles_actions( role_id, action_id ) values ( 92, 'products/ns/expressCalc' );

--rollback delete from roles_actions where action_id = 'products/ns/expressCalc';

--rollback delete from action_params where action_id = 'products/ns/expressCalc';

--rollback delete from actions where id = 'products/ns/expressCalc';

--rollback delete
--rollback from tparams
--rollback where id in ( 'term-begin', 'term-end', 'coverage-risks-risk-id', 'coverage-risks-risk-value', 'attributes-attribute-id', 'attributes-attribute-value', 'insurants-insurant' )
--rollback and not exists ( select *
--rollback from action_params
--rollback where param_id in ( 'term-begin', 'term-end', 'coverage-risks-risk-id', 'coverage-risks-risk-value', 'attributes-attribute-id', 'attributes-attribute-value', 'insurants-insurant' ));
