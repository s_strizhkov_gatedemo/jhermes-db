--liquibase formatted sql
--changeset bogomolov:initial_public_table
CREATE SEQUENCE action_params_id_seq;

CREATE SEQUENCE logic_params_id_seq;

CREATE TABLE action_params (id BIGSERIAL NOT NULL, action_id VARCHAR NOT NULL, param_id VARCHAR NOT NULL, is_required BOOLEAN NOT NULL, CONSTRAINT action_params_pkey1 PRIMARY KEY (id));

CREATE TABLE actions (id VARCHAR(50) NOT NULL, actiontype_id VARCHAR NOT NULL, descr VARCHAR(200) NOT NULL, dependant VARCHAR NOT NULL, flatten_params BOOLEAN DEFAULT false NOT NULL);
COMMENT ON TABLE actions IS 'Действия';
COMMENT ON COLUMN actions.id IS 'Ключ-литерал, регистро-независимый';
COMMENT ON COLUMN actions.actiontype_id IS 'Тип действия';
COMMENT ON COLUMN actions.flatten_params IS 'Метка о том, что действие ожидает на вход плоскую структуру параметров';

CREATE TABLE actiontypes (id VARCHAR(20) NOT NULL, descr VARCHAR(250) NOT NULL);
COMMENT ON TABLE actiontypes IS 'Типы действий';
COMMENT ON COLUMN actiontypes.id IS 'Ключ-литерал';
COMMENT ON COLUMN actiontypes.descr IS 'Описание';

CREATE TABLE errors (code INT NOT NULL, text VARCHAR(200) NOT NULL);

CREATE TABLE params (id VARCHAR NOT NULL, kias_name VARCHAR NOT NULL, descr VARCHAR NOT NULL, type_id VARCHAR NOT NULL, public_name VARCHAR DEFAULT '1' NOT NULL, maxlength INT, mask VARCHAR);
COMMENT ON TABLE params IS 'Параметры вызова действия';
COMMENT ON COLUMN params.descr IS 'Описание параметра';
COMMENT ON COLUMN params.type_id IS 'Тип параметра';
COMMENT ON COLUMN params.maxlength IS 'Максимальная длина значения параметра';
COMMENT ON COLUMN params.mask IS 'Маска значений параметра';

CREATE TABLE paramtypes (id VARCHAR(50) NOT NULL, descr VARCHAR(100) NOT NULL);

CREATE TABLE roles (id SERIAL NOT NULL, name VARCHAR(40) NOT NULL, descr VARCHAR(250) NOT NULL, CONSTRAINT roles_pkey PRIMARY KEY (id));

CREATE TABLE roles_actions (id SERIAL NOT NULL, role_id INT NOT NULL, action_id VARCHAR(50) NOT NULL, CONSTRAINT roles_services_pkey PRIMARY KEY (id));
COMMENT ON TABLE roles_actions IS 'Какие действия (actions) могут вызываться ролью';

CREATE TABLE services (id VARCHAR(60) NOT NULL, "isDict" BOOLEAN DEFAULT FALSE NOT NULL);
COMMENT ON TABLE services IS 'Переименовать в services_extra, хранить здесь свойства, характерные для сущностей сервисов веб-службы (напр., isDict)';
COMMENT ON COLUMN services."isDict" IS 'Возвращает ли метод справочник (подлежащий кэшированию)';

CREATE TABLE user_defined_params (id BIGSERIAL NOT NULL, role_id INT, user_id INT, action_id VARCHAR NOT NULL, param_id VARCHAR NOT NULL, value VARCHAR NOT NULL, CONSTRAINT pk_user_defined_params PRIMARY KEY (id));
COMMENT ON TABLE user_defined_params IS 'Параметры, автоматически добавляемые к вызову действия';

CREATE TABLE user_roles (id SERIAL NOT NULL, user_id INT NOT NULL, role_id INT NOT NULL, CONSTRAINT user_role_pkey PRIMARY KEY (id));
COMMENT ON TABLE user_roles IS 'Связь пользователь-роль';

CREATE TABLE users (id INT NOT NULL, username VARCHAR, password VARCHAR, expiredate date, active BOOLEAN DEFAULT TRUE NOT NULL, token VARCHAR NOT NULL, descr VARCHAR(150) NOT NULL, kiasuser_id INT NOT NULL);
COMMENT ON COLUMN users.active IS 'поле блокировки';

ALTER TABLE actions ADD CONSTRAINT actions_pkey PRIMARY KEY (id);

ALTER TABLE actiontypes ADD CONSTRAINT actiontypes_pkey PRIMARY KEY (id);

ALTER TABLE errors ADD CONSTRAINT errors_pkey PRIMARY KEY (code);

ALTER TABLE params ADD CONSTRAINT params_pkey PRIMARY KEY (id);

ALTER TABLE paramtypes ADD CONSTRAINT paramtypes_pkey PRIMARY KEY (id);

ALTER TABLE services ADD CONSTRAINT services_pkey PRIMARY KEY (id);

ALTER TABLE users ADD CONSTRAINT users_pkey PRIMARY KEY (id);

ALTER TABLE action_params ADD CONSTRAINT uk_action_param UNIQUE (action_id, param_id);

ALTER TABLE users ADD CONSTRAINT uk_token UNIQUE (token);

ALTER TABLE user_defined_params ADD CONSTRAINT uk_user_action_param UNIQUE (role_id, user_id, action_id, param_id);

CREATE INDEX users_id_idx ON users(id);

CREATE INDEX users_username_idx ON users(username);

ALTER TABLE action_params ADD CONSTRAINT fk_action FOREIGN KEY (action_id) REFERENCES actions (id) ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE user_defined_params ADD CONSTRAINT fk_action_id FOREIGN KEY (action_id) REFERENCES actions (id) ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE actions ADD CONSTRAINT fk_actiontype FOREIGN KEY (actiontype_id) REFERENCES actiontypes (id) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE users ADD CONSTRAINT fk_kiasuser FOREIGN KEY (kiasuser_id) REFERENCES env.kiasappusers (id) ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE action_params ADD CONSTRAINT fk_param FOREIGN KEY (param_id) REFERENCES params (id) ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE user_defined_params ADD CONSTRAINT fk_param_id FOREIGN KEY (param_id) REFERENCES params (id) ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE params ADD CONSTRAINT fk_paramtypes FOREIGN KEY (type_id) REFERENCES paramtypes (id) ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE user_roles ADD CONSTRAINT fk_role FOREIGN KEY (role_id) REFERENCES roles (id) ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE user_roles ADD CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE user_defined_params ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE CASCADE ON DELETE NO ACTION;

CREATE VIEW v_action_params AS  SELECT a.id AS action_id,
    p.id AS param_id,
    p.public_name,
    p.kias_name,
    p.descr,
    p.type_id,
    p.maxlength,
    p.mask,
    ap.is_required
   FROM params p,
    actions a,
    action_params ap
  WHERE (((p.id)::text = (ap.param_id)::text) AND ((a.id)::text = (ap.action_id)::text));;

CREATE VIEW v_user_actions AS  SELECT u.id AS user_id,
    r.name AS role_id,
    a.id AS action_id
   FROM users u,
    user_roles ur,
    roles r,
    roles_actions ar,
    actions a
  WHERE ((u.id = ur.user_id) AND (r.id = ur.role_id) AND (r.id = ar.role_id) AND ((a.id)::text = (ar.action_id)::text));;
