--liquibase formatted sql
--changeset bogomolov:create_parent_id
-- Создадим новую таблицу tparams как копию params и добавим в нее новое поле parent_id
CREATE TABLE tparams AS SELECT *
FROM params;

ALTER TABLE tparams ADD COLUMN parent_id CHARACTER VARYING;

COMMENT ON
TABLE public.tparams IS 'Параметры вызова действия' ;

COMMENT ON
COLUMN public.tparams.descr IS 'Описание параметра' ;

COMMENT ON
COLUMN public.tparams.type_id IS 'Тип параметра' ;

COMMENT ON
COLUMN public.tparams.maxlength IS 'Максимальная длина значения параметра' ;

COMMENT ON
COLUMN public.tparams.mask IS 'Маска значений параметра' ;

COMMENT ON
COLUMN public.tparams.parent_id IS 'ID родительского параметра' ;

-- Удалим ссылки на таблицу params
ALTER TABLE action_params DROP CONSTRAINT fk_param;

ALTER TABLE user_defined_params DROP CONSTRAINT fk_param_id;

-- Удалим view v_action_params
DROP VIEW v_action_params;

-- Удалим саму таблицу params
DROP TABLE params;

ALTER TABLE tparams ADD CONSTRAINT params_pkey PRIMARY KEY ( id );

ALTER TABLE tparams ADD CONSTRAINT fk_paramtypes FOREIGN KEY ( type_id ) REFERENCES paramtypes( id ) ON
UPDATE CASCADE;

-- Создадим view params в которой выведем значения из tparams, в которой будем заполнять значения потоков из значений родителей
CREATE
OR REPLACE VIEW public.params AS WITH RECURSIVE t AS ( SELECT id, kias_name, descr, type_id, public_name, maxlength, mask, parent_id
FROM tparams
WHERE parent_id IS NULL
UNION SELECT tt.id, COALESCE( tt.kias_name, t.kias_name ) kias_name, COALESCE( tt.descr, t.descr ) descr, COALESCE( tt.type_id, t.type_id ) type_id, COALESCE( tt.public_name, t.public_name ) public_name, COALESCE( tt.maxlength, t.maxlength ) maxlength, COALESCE( tt.mask, t.mask ) mask, tt.parent_id
FROM tparams tt
JOIN t ON
tt.parent_id = t.id ) SELECT *
FROM t
ORDER BY id;

ALTER TABLE public.params OWNER TO jhermes;

-- Добавим для таблиц удаленные ранее ссылки на новую таблицу tparams
ALTER TABLE action_params ADD CONSTRAINT fk_param FOREIGN KEY ( param_id ) REFERENCES tparams( id ) ON
UPDATE CASCADE;

ALTER TABLE user_defined_params ADD CONSTRAINT fk_param_id FOREIGN KEY ( param_id ) REFERENCES tparams( id ) ON
UPDATE CASCADE;

-- Пересоздадим view v_action_params
CREATE
OR REPLACE VIEW public.v_action_params AS SELECT a.id AS action_id, p.id AS param_id, p.public_name, p.kias_name, p.descr, p.type_id, p.maxlength, p.mask, ap.is_required
FROM params p, actions a, action_params ap
WHERE p.id::TEXT = ap.param_id::TEXT
AND a.id::TEXT = ap.action_id::TEXT;

ALTER TABLE public.v_action_params OWNER TO jhermes;

--rollback DROP VIEW v_action_params;

--rollback DROP VIEW params;

--rollback CREATE TABLE params AS SELECT * FROM tparams;

--rollback ALTER TABLE params DROP COLUMN parent_id;

--rollback COMMENT ON TABLE public.params IS 'Параметры вызова действия' ;

--rollback COMMENT ON COLUMN public.params.descr IS 'Описание параметра' ;

--rollback COMMENT ON COLUMN public.params.type_id IS 'Тип параметра' ;

--rollback COMMENT ON COLUMN public.params.maxlength IS 'Максимальная длина значения параметра' ;

--rollback COMMENT ON COLUMN public.params.mask IS 'Маска значений параметра' ;

--rollback ALTER TABLE action_params DROP CONSTRAINT fk_param;

--rollback ALTER TABLE user_defined_params DROP CONSTRAINT fk_param_id;

--rollback DROP TABLE tparams;

--rollback ALTER TABLE params ADD CONSTRAINT params_pkey PRIMARY KEY ( id );

--rollback ALTER TABLE params ADD CONSTRAINT fk_paramtypes FOREIGN KEY ( type_id ) REFERENCES paramtypes( id ) ON UPDATE CASCADE;

--rollback CREATE OR REPLACE VIEW public.v_action_params AS SELECT a.id AS action_id, p.id AS param_id, p.public_name, p.kias_name, p.descr, p.type_id, p.maxlength, p.mask, ap.is_required FROM params p, actions a,  action_params ap WHERE p.id::TEXT = ap.param_id::TEXT AND a.id::TEXT = ap.action_id::TEXT;

--rollback ALTER TABLE public.v_action_params OWNER TO jhermes;

--rollback ALTER TABLE action_params ADD CONSTRAINT fk_param FOREIGN KEY ( param_id ) REFERENCES params( id ) ON UPDATE CASCADE;

--rollback ALTER TABLE user_defined_params ADD CONSTRAINT fk_param_id FOREIGN KEY ( param_id ) REFERENCES params( id ) ON UPDATE CASCADE;
