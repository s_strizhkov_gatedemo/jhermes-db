--liquibase formatted sql
--changeset bogomolov:my_apartment_online_full_calc
insert into actions( id, actiontype_id, descr, dependant, flatten_params )
values ( 'products/estate/apartment/createCalc', 'call', 'Расчет полной котировки Моя квартира-Онлайн', 'USER_MyApartmentFullCalcCreate', false );

 insert into tparams( id, kias_name, descr, type_id, public_name, maxlength, mask, parent_id ) values
( 'productId', 'productId', 'Уникальный идентификатор продукта', 'string', 'productId', null, null, null ),
( 'insuranceObject-address-region-custom_value', 'insuranceObject-address-region-custom_value', 'Адрес застрахованного имущества: пользовательское значение региона', 'string', 'insuranceObject-address-region-custom_value', null, null, null ),
( 'insuranceObject-address-region-name', 'insuranceObject-address-region-name', 'Адрес застрахованного имущества: Регион РФ', 'string', 'insuranceObject-address-region-name', null, null, null ),
( 'insuranceObject-address-region-abbr', 'insuranceObject-address-region-abbr', 'Адрес застрахованного имущества: абревиатура региона', 'string', 'insuranceObject-address-region-abbr', null, null, null ),
( 'insuranceObject-address-region-code', 'insuranceObject-address-region-code', 'Адрес застрахованного имущества: код региона', 'string', 'insuranceObject-address-region-code', null, null, null ),
( 'insuranceObject-address-district-custom_value', 'insuranceObject-address-district-custom_value', 'Адрес застрахованного имущества: пользовательское значение района', 'string', 'insuranceObject-address-district-custom_value', null, null, null ),
( 'insuranceObject-address-city-custom_value', 'insuranceObject-address-city-custom_value', 'Адрес застрахованного имущества: пользовательское значение города', 'string', 'insuranceObject-address-city-custom_value', null, null, null ),
( 'insuranceObject-address-city-name', 'insuranceObject-address-city-name', 'Адрес застрахованного имущества: город', 'string', 'insuranceObject-address-city-name', null, null, null ),
( 'insuranceObject-address-city-abbr', 'insuranceObject-address-city-abbr', 'Адрес застрахованного имущества: абревиатура города', 'string', 'insuranceObject-address-city-abbr', null, null, null ),
( 'insuranceObject-address-city-code', 'insuranceObject-address-city-code', 'Адрес застрахованного имущества: код города', 'string', 'insuranceObject-address-city-code', null, null, null ),
( 'insuranceObject-address-locality-custom_value', 'insuranceObject-address-locality-custom_value', 'Адрес застрахованного имущества: пользовательское значение местности', 'string', 'insuranceObject-address-locality-custom_value', null, null, null ),
( 'insuranceObject-address-street-custom_value', 'insuranceObject-address-street-custom_value', 'Адрес застрахованного имущества: пользовательское значение города', 'string', 'insuranceObject-address-street-custom_value', null, null, null ),
( 'insuranceObject-address-street-name', 'insuranceObject-address-street-name', 'Адрес застрахованного имущества: улица', 'string', 'insuranceObject-address-street-name', null, null, null ),
( 'insuranceObject-address-street-abbr', 'insuranceObject-address-street-abbr', 'Адрес застрахованного имущества: абревиатура улицы', 'string', 'insuranceObject-address-street-abbr', null, null, null ),
( 'insuranceObject-address-street-code', 'insuranceObject-address-street-code', 'Адрес застрахованного имущества: код улицы', 'string', 'insuranceObject-address-street-code', null, null, null ),
( 'insuranceObject-address-house', 'insuranceObject-address-house', null, null, 'insuranceObject-address-house', null, null, 'insurant-address-house' ),
( 'insuranceObject-address-building', 'insuranceObject-address-building', null, null, 'insuranceObject-address-building', null, null, 'insurant-address-housing' ),
( 'insuranceObject-address-flat', 'insuranceObject-address-flat', null, null, 'insuranceObject-address-flat', null, null, 'insurant-address-apartment' ),
( 'insuranceObject-address-postcode', 'insuranceObject-address-postcode', 'Адрес застрахованного имущества: Индекс', null, 'insuranceObject-address-postcode', null, null, 'insurer-address-postcode' );

insert into action_params( action_id, param_id, is_required ) values
( 'products/estate/apartment/createCalc', 'isn', false ),
( 'products/estate/apartment/createCalc', 'calcId', false ),
( 'products/estate/apartment/createCalc', 'premium', false ),
( 'products/estate/apartment/createCalc', 'productId', true ),
( 'products/estate/apartment/createCalc', 'promocode', false ),
( 'products/estate/apartment/createCalc', 'term-begin', true ),
( 'products/estate/apartment/createCalc', 'term-end', true ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-region-custom_value', true ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-region-name', false ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-region-abbr', false ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-region-code', false ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-district-custom_value', false ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-city-custom_value', true ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-city-name', false ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-city-abbr', false ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-city-code', false ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-locality-custom_value', false ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-street-custom_value', true ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-street-name', false ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-street-abbr', false ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-street-code', false ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-house', true ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-building', false ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-flat', true ),
( 'products/estate/apartment/createCalc', 'insuranceObject-address-postcode', false ),
( 'products/estate/apartment/createCalc', 'insurer-isn', false ),
( 'products/estate/apartment/createCalc', 'insurer-firstName', true ),
( 'products/estate/apartment/createCalc', 'insurer-middleName', false ),
( 'products/estate/apartment/createCalc', 'insurer-lastName', true ),
( 'products/estate/apartment/createCalc', 'insurer-birthDate', true ),
( 'products/estate/apartment/createCalc', 'insurer-phone-ns', true ),
( 'products/estate/apartment/createCalc', 'insurer-email', true ),
( 'products/estate/apartment/createCalc', 'insurer-gender', true ),
( 'products/estate/apartment/createCalc', 'insurer-documents-document-type', true ),
( 'products/estate/apartment/createCalc', 'insurer-documents-document-series', true ),
( 'products/estate/apartment/createCalc', 'insurer-documents-document-number', true ),
( 'products/estate/apartment/createCalc', 'insurer-documents-document-date', true ),
( 'products/estate/apartment/createCalc', 'insurer-documents-document-issuedBy', true ),
( 'products/estate/apartment/createCalc', 'insurer-address-region-custom_value', true ),
( 'products/estate/apartment/createCalc', 'insurer-address-region-name', false ),
( 'products/estate/apartment/createCalc', 'insurer-address-region-abbr', false ),
( 'products/estate/apartment/createCalc', 'insurer-address-region-code', false ),
( 'products/estate/apartment/createCalc', 'insurer-address-district-custom_value', false ),
( 'products/estate/apartment/createCalc', 'insurer-address-city-custom_value', true ),
( 'products/estate/apartment/createCalc', 'insurer-address-city-name', false ),
( 'products/estate/apartment/createCalc', 'insurer-address-city-abbr', false ),
( 'products/estate/apartment/createCalc', 'insurer-address-city-code', false ),
( 'products/estate/apartment/createCalc', 'insurer-address-locality-custom_value', false ),
( 'products/estate/apartment/createCalc', 'insurer-address-street-custom_value', true ),
( 'products/estate/apartment/createCalc', 'insurer-address-street-name', false ),
( 'products/estate/apartment/createCalc', 'insurer-address-street-abbr', false ),
( 'products/estate/apartment/createCalc', 'insurer-address-street-code', false ),
( 'products/estate/apartment/createCalc', 'insurer-address-house', true ),
( 'products/estate/apartment/createCalc', 'insurer-address-building', false ),
( 'products/estate/apartment/createCalc', 'insurer-address-flat', false ),
( 'products/estate/apartment/createCalc', 'insurer-address-postcode', false ),
( 'products/estate/apartment/createCalc', 'attributes-attribute-id', true ),
( 'products/estate/apartment/createCalc', 'attributes-attribute-value', true ),
( 'products/estate/apartment/createCalc', 'clauses-clause-id', true ),
( 'products/estate/apartment/createCalc', 'clauses-clause-value', true );

insert into roles_actions( role_id, action_id ) values ( 91, 'products/estate/apartment/createCalc' );

--rollback delete from roles_actions where action_id = 'products/estate/apartment/createCalc';

--rollback delete from action_params where action_id = 'products/estate/apartment/createCalc';

--rollback delete from actions where id = 'products/estate/apartment/createCalc';

--rollback delete
--rollback from tparams
--rollback where id in ( 'productId', 'insuranceObject-address-region-custom_value', 'insuranceObject-address-region-name', 'insuranceObject-address-region-abbr', 'insuranceObject-address-region-code', 'insuranceObject-address-district-custom_value', 'insuranceObject-address-city-custom_value', 'insuranceObject-address-city-name', 'insuranceObject-address-city-abbr', 'insuranceObject-address-city-code', 'insuranceObject-address-locality-custom_value', 'insuranceObject-address-street-custom_value', 'insuranceObject-address-street-name', 'insuranceObject-address-street-abbr', 'insuranceObject-address-street-code', 'insuranceObject-address-house', 'insuranceObject-address-building', 'insuranceObject-address-flat', 'insuranceObject-address-postcode' )
--rollback and not exists ( select *
--rollback from action_params
--rollback where param_id in ( 'productId', 'insuranceObject-address-region-custom_value', 'insuranceObject-address-region-name', 'insuranceObject-address-region-abbr', 'insuranceObject-address-region-code', 'insuranceObject-address-district-custom_value', 'insuranceObject-address-city-custom_value', 'insuranceObject-address-city-name', 'insuranceObject-address-city-abbr', 'insuranceObject-address-city-code', 'insuranceObject-address-locality-custom_value', 'insuranceObject-address-street-custom_value', 'insuranceObject-address-street-name', 'insuranceObject-address-street-abbr', 'insuranceObject-address-street-code', 'insuranceObject-address-house', 'insuranceObject-address-building', 'insuranceObject-address-flat', 'insuranceObject-address-postcode' ));
