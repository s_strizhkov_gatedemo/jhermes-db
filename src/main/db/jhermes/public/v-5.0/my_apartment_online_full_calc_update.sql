--liquibase formatted sql
--changeset bogomolov:my_apartment_online_full_calc_update
insert into actions( id, actiontype_id, descr, dependant, flatten_params )
values ( 'products/estate/apartment/updateCalc', 'call', 'Обновление полной котировки Моя квартира-Онлайн', 'USER_MyApartmentFullCalcUpdate', false );

insert into action_params( action_id, param_id, is_required ) values
( 'products/estate/apartment/updateCalc', 'isn', true ),
( 'products/estate/apartment/updateCalc', 'calcId', true ),
( 'products/estate/apartment/updateCalc', 'premium', false ),
( 'products/estate/apartment/updateCalc', 'productId', true ),
( 'products/estate/apartment/updateCalc', 'promocode', false ),
( 'products/estate/apartment/updateCalc', 'term-begin', true ),
( 'products/estate/apartment/updateCalc', 'term-end', true ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-region-custom_value', true ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-region-name', false ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-region-abbr', false ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-region-code', false ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-district-custom_value', false ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-city-custom_value', true ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-city-name', false ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-city-abbr', false ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-city-code', false ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-locality-custom_value', false ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-street-custom_value', true ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-street-name', false ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-street-abbr', false ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-street-code', false ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-house', true ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-building', false ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-flat', true ),
( 'products/estate/apartment/updateCalc', 'insuranceObject-address-postcode', false ),
( 'products/estate/apartment/updateCalc', 'insurer-isn', true ),
( 'products/estate/apartment/updateCalc', 'insurer-firstName', true ),
( 'products/estate/apartment/updateCalc', 'insurer-middleName', false ),
( 'products/estate/apartment/updateCalc', 'insurer-lastName', true ),
( 'products/estate/apartment/updateCalc', 'insurer-birthDate', true ),
( 'products/estate/apartment/updateCalc', 'insurer-phone-ns', true ),
( 'products/estate/apartment/updateCalc', 'insurer-email', true ),
( 'products/estate/apartment/updateCalc', 'insurer-gender', true ),
( 'products/estate/apartment/updateCalc', 'insurer-documents-document-type', true ),
( 'products/estate/apartment/updateCalc', 'insurer-documents-document-series', true ),
( 'products/estate/apartment/updateCalc', 'insurer-documents-document-number', true ),
( 'products/estate/apartment/updateCalc', 'insurer-documents-document-date', true ),
( 'products/estate/apartment/updateCalc', 'insurer-documents-document-issuedBy', true ),
( 'products/estate/apartment/updateCalc', 'insurer-address-region-custom_value', true ),
( 'products/estate/apartment/updateCalc', 'insurer-address-region-name', false ),
( 'products/estate/apartment/updateCalc', 'insurer-address-region-abbr', false ),
( 'products/estate/apartment/updateCalc', 'insurer-address-region-code', false ),
( 'products/estate/apartment/updateCalc', 'insurer-address-district-custom_value', false ),
( 'products/estate/apartment/updateCalc', 'insurer-address-city-custom_value', true ),
( 'products/estate/apartment/updateCalc', 'insurer-address-city-name', false ),
( 'products/estate/apartment/updateCalc', 'insurer-address-city-abbr', false ),
( 'products/estate/apartment/updateCalc', 'insurer-address-city-code', false ),
( 'products/estate/apartment/updateCalc', 'insurer-address-locality-custom_value', false ),
( 'products/estate/apartment/updateCalc', 'insurer-address-street-custom_value', true ),
( 'products/estate/apartment/updateCalc', 'insurer-address-street-name', false ),
( 'products/estate/apartment/updateCalc', 'insurer-address-street-abbr', false ),
( 'products/estate/apartment/updateCalc', 'insurer-address-street-code', false ),
( 'products/estate/apartment/updateCalc', 'insurer-address-house', true ),
( 'products/estate/apartment/updateCalc', 'insurer-address-building', false ),
( 'products/estate/apartment/updateCalc', 'insurer-address-flat', false ),
( 'products/estate/apartment/updateCalc', 'insurer-address-postcode', false ),
( 'products/estate/apartment/updateCalc', 'attributes-attribute-id', true ),
( 'products/estate/apartment/updateCalc', 'attributes-attribute-value', true ),
( 'products/estate/apartment/updateCalc', 'clauses-clause-id', true ),
( 'products/estate/apartment/updateCalc', 'clauses-clause-value', true );

insert into roles_actions( role_id, action_id ) values ( 91, 'products/estate/apartment/updateCalc' );

--rollback delete from roles_actions where action_id = 'products/estate/apartment/updateCalc';

--rollback delete from action_params where action_id = 'products/estate/apartment/updateCalc';

--rollback delete from actions where id = 'products/estate/apartment/updateCalc';

--rollback delete
--rollback from tparams
--rollback where id in ( 'productId', 'insuranceObject-address-region-custom_value', 'insuranceObject-address-region-name', 'insuranceObject-address-region-abbr', 'insuranceObject-address-region-code', 'insuranceObject-address-district-custom_value', 'insuranceObject-address-city-custom_value', 'insuranceObject-address-city-name', 'insuranceObject-address-city-abbr', 'insuranceObject-address-city-code', 'insuranceObject-address-locality-custom_value', 'insuranceObject-address-street-custom_value', 'insuranceObject-address-street-name', 'insuranceObject-address-street-abbr', 'insuranceObject-address-street-code', 'insuranceObject-address-house', 'insuranceObject-address-building', 'insuranceObject-address-flat', 'insuranceObject-address-postcode' )
--rollback and not exists ( select *
--rollback from action_params
--rollback where param_id in ( 'productId', 'insuranceObject-address-region-custom_value', 'insuranceObject-address-region-name', 'insuranceObject-address-region-abbr', 'insuranceObject-address-region-code', 'insuranceObject-address-district-custom_value', 'insuranceObject-address-city-custom_value', 'insuranceObject-address-city-name', 'insuranceObject-address-city-abbr', 'insuranceObject-address-city-code', 'insuranceObject-address-locality-custom_value', 'insuranceObject-address-street-custom_value', 'insuranceObject-address-street-name', 'insuranceObject-address-street-abbr', 'insuranceObject-address-street-code', 'insuranceObject-address-house', 'insuranceObject-address-building', 'insuranceObject-address-flat', 'insuranceObject-address-postcode' ));
